if [ $# -eq 0 ]; then
  me=`basename "$0"`
  echo "An Output folder are required to export split data"
  echo "USAGE: $me <FOLDER>"
  echo "e.g. $me /tmp/spotprices"
  exit 1
fi

DIRECTORY=$1
if [ ! -d "$DIRECTORY" ]; then
  mkdir $DIRECTORY
fi

SPLIT_ZIP_DIR=$DIRECTORY/data
if [ ! -d "$SPLIT_ZIP_DIR" ]; then
  mkdir $SPLIT_ZIP_DIR
fi

SPLIT_CSV_DIR=$DIRECTORY/export
if [ ! -d "$SPLIT_CSV_DIR" ]; then
  mkdir $SPLIT_CSV_DIR
fi

year=2017
for month in $(seq -f "%02g" 4 12); do
	LOCAL_FILENAME=$DIRECTORY/spotprices_$year-$month.csv
	echo "Generating CSV files to $year / $month"
	psql aws -c "COPY (SELECT * FROM spotprice_all WHERE to_char(timestamp, 'YYYY-MM') = '$year-$month') TO '$LOCAL_FILENAME' CSV HEADER"
	split -b 10m -a 2 $LOCAL_FILENAME $SPLIT_CSV_DIR/spotprices_$year-$month
	for temp in $SPLIT_CSV_DIR/spotprices_$year-$month*; do
		mv "$temp" $temp.csv
	done
	zip -s 10 $SPLIT_ZIP_DIR/spotprices_$year-$month $LOCAL_FILENAME
	rm -rf $LOCAL_FILENAME
done

year=2018
for month in $(seq -f "%02g" 1 11); do
	LOCAL_FILENAME=$DIRECTORY/spotprices_$year-$month.csv
	echo "Generating CSV files to $year / $month"
	psql aws -c "COPY (SELECT * FROM spotprice_all WHERE to_char(timestamp, 'YYYY-MM') = '$year-$month') TO '$LOCAL_FILENAME' CSV HEADER"
	split -b 10m -a 2 $LOCAL_FILENAME $SPLIT_CSV_DIR/spotprices_$year-$month
	for temp in $SPLIT_CSV_DIR/spotprices_$year-$month*; do
		mv "$temp" $temp.csv
	done
	zip -s 10 $SPLIT_ZIP_DIR/spotprices_$year-$month $LOCAL_FILENAME
	rm -rf $LOCAL_FILENAME
done
