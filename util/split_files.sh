if [ $# -eq 0 ]; then
  me=`basename "$0"`
  echo "An source folder are required to export split data"
  echo "USAGE: $me <FOLDER>"
  echo "e.g. $me /tmp/spotprices"
  exit 1
fi

DIRECTORY=$1

SPLIT_ZIP_DIR=$DIRECTORY/data
if [ ! -d "$SPLIT_ZIP_DIR" ]; then
  mkdir $SPLIT_ZIP_DIR
fi

for file in $DIRECTORY/*; do
	filename=${file##*/}
	echo "Spliting $filename"
	split -b 10m -a 2 $file $SPLIT_ZIP_DIR/${filename%.*}-
done


for temp in $SPLIT_ZIP_DIR/*; do
	mv "$temp" $temp.csv
done
